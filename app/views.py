from django.http import HttpResponse
from django.shortcuts import render, redirect

from . import models
from .forms import NewNoteForm

# Create your views here.
def index(request):
    notes = models.Note.objects.all()
    
    isNote = False
    
    if notes:
        isNote = True
    
    context = {
        'notes': notes,
        'isNote': isNote
    }
    return render(request, 'index.html', context)

def new_note(request):
    form = NewNoteForm()
    if request.method == 'POST':
        form = NewNoteForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            text = form.cleaned_data['text']
            models.Note(title=title, text=text).save()
            redirect('index')
    return render(request, 'new_note.html', {'form': form})